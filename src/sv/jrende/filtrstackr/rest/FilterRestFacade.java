package sv.jrende.filtrstackr.rest;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.SchemaOutputResolver;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.SchemaFactory;

import sv.jrende.filtrstackr.control.AvailableFilters;
import sv.jrende.filtrstackr.model.FilterDefinition;
import sv.jrende.filtrstackr.model.FilterDefinitionList;

//import sv.jrende.filtrstackr.control.AvailableFilters;
//import sv.jrende.filtrstackr.json.FilterDefinition;

@Stateless
@Path("/filters")
public class FilterRestFacade {
	@EJB
	AvailableFilters availableFilters;
	
	public FilterRestFacade() {
		// TODO Auto-generated constructor stub
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public FilterDefinitionList getFilters() {
		FilterDefinitionList f = new FilterDefinitionList();
		f.setFilterList(new ArrayList<FilterDefinition>());
		for (FilterDefinition filter : availableFilters.getFilterList().getFilterList()) {
			f.getFilterList().add(filter);
		}
		return f;
	}
	
//	@POST
//	@Produces("text/plain")
//	@Consumes({"text/plain"})
//	public String getPicture(@FormParam(value = "filters") String test) {
//		return test;
//	}
	@POST
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
//	@Consumes("text/plain")
	@Produces("text/plain")
	public String getPicture(@FormParam(value = "filters") FilterDefinitionList filterList) {
		System.out.println("getPicture called.");
		if (filterList != null) {
			for (FilterDefinition filterDefinition : filterList.getFilterList()) {
				System.out.println(filterDefinition.getName());
			}
			return "Lyckat anrop!";
		} else {
			return "Null...";
		}
	}
}
