package sv.jrende.filtrstackr.control;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

import sv.jrende.filtrstackr.model.FilterDefinition;
import sv.jrende.filtrstackr.model.FilterDefinitionList;


@Stateless
public class AvailableFilters {
	FilterDefinitionList filterList;

	public AvailableFilters() {
		System.out.println("Tjo");
	}

	@PostConstruct
	public void init() {
		filterList = new FilterDefinitionList();
		filterList.setFilterList(new ArrayList<FilterDefinition>());
		FilterDefinition e;
		e = new FilterDefinition();
		e.setName("clouds");
		Map<String, Object> m = new HashMap<String, Object>();
		m.put("seed", 0.25354);
		e.setFields(m);
		filterList.getFilterList().add(e);
		
		e = new FilterDefinition();
		e.setName("contrast");
		m = new HashMap<String, Object>();
		m.put("seed", 1231);
		e.setFields(m);
		filterList.getFilterList().add(e);
		
		e = new FilterDefinition();
		e.setName("Test");
		m = new HashMap<String, Object>();
		m.put("value", 0.25354);
		m.put("x", 0.25354);
		m.put("y", 0.25354);
		m.put("dX", 3.123);
		m.put("dY", 2.123);
		m.put("test", true);
		e.setFields(m);
		filterList.getFilterList().add(e);
	}

	public FilterDefinitionList getFilterList() {
		return filterList;
	}

	public void setFilterList(FilterDefinitionList filterList) {
		this.filterList = filterList;
	}
}
