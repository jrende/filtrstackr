package sv.jrende.filtrstackr.model;

import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FilterDefinition {
	String name;
	Map<String, Object> fields;

	public FilterDefinition() {
		// TODO Auto-generated constructor stub
	}

	public FilterDefinition(String name, Map<String, Object> fields) {
		super();
		this.name = name;
		this.fields = fields;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, Object> getFields() {
		return fields;
	}

	public void setFields(Map<String, Object> fields) {
		this.fields = fields;
	}

}
