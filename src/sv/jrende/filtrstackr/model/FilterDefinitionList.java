package sv.jrende.filtrstackr.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FilterDefinitionList {
//	 @XmlElement
	List<FilterDefinition> filterList;

	public FilterDefinitionList() {
		// TODO Auto-generated constructor stub
	}

	public List<FilterDefinition> getFilterList() {
		return filterList;
	}

	public void setFilterList(List<FilterDefinition> filterList) {
		this.filterList = filterList;
	}

}
