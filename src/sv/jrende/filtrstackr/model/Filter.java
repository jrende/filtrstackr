package sv.jrende.filtrstackr.model;

import java.awt.Image;
import java.io.Serializable;



public abstract class Filter implements Serializable {
	FilterDefinition id;

	public Image doOperation(Image img) {
		return img;
	}

	public FilterDefinition getId() {
		return id;
	}

	public void setId(FilterDefinition id) {
		this.id = id;
	}
	
}
