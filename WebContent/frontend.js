filterData = {};
jQuery.get("./api/filters", function(response, status, xhr) {
	if (status == "error") {
		var msg = "Sorry but there was an error: ";
		$("#error").html(msg + xhr.status + " " + xhr.statusText);
	}
	filterData = response;
});
$(document).ready(function() {
	var mycontenttype = "application/json; charset=utf-8";
//	var mycontenttype = "text/plain; charset=utf-8";
	$("#makeCallWrapped").on("click", function() {
		$.ajax({
			url : "./api/filters",
			type : "POST",
			data : {"filters": JSON.stringify(filterData)},
			contentType : mycontenttype,
			dataType : "json",
			success : successFunction,
			error : errorFunction,
		});
	});
	
	$("#makeCallUnWrapped").on("click", function() {
		$.ajax({
			url : "./api/filters",
			type : "POST",
			data : JSON.stringify(filterData),
			contentType : mycontenttype,
			dataType : "json",
			success : successFunction,
			error : errorFunction,
		});
	});
	
	$("#getJSON").on("click", function() {
		document.write(JSON.stringify(filterData));
	});
});
var successFunction = function(response, textStatus, jqHXR) {
	console.log("Success");
	a = response;
	b = textStatus;
	c = jqHXR;
	$("#contents").text("");
	$("#contents").text(textStatus + ": " + response);
};

var errorFunction = function(response, textStatus, jqHXR) {
	console.log("error");
	test = response;
	$("#contents").text(textStatus + ": " + response.responseText);
};